package main

import (
  "strings"
  "os"
  "io/ioutil"
  "net/http"
  "encoding/json"
  "fmt"
  "time"
  "syscall"

  "github.com/ruedap/go-alfred"
)

type Repo struct {
  Name string `json:"name_with_namespace"`
  Description string `json:"description"`
  WebUrl string `json:"web_url"`
}

func check(e error) {
  if e != nil {
    panic(e)
  }
}

func main() {

  if len(os.Args) < 2 {
    os.Exit(1)
  }

  program_args := strings.Join(os.Args[1:], " ")

  resp := alfred.NewResponse()

  home := os.Getenv("HOME")

  cache_path := home + "/Library/Caches/com.runningwithcrayons.Alfred-2/Workflow Data/codehog.gitlab/cache"

  var st syscall.Stat_t
  err := syscall.Stat(cache_path, &st)
  cache_ctime := st.Ctimespec.Sec

  now := time.Now().Unix()

  cache, err := ioutil.ReadFile(cache_path)

  var r []Repo
  println(now - cache_ctime)
  if len(cache) > 0 && (now - cache_ctime) < 21600 {
    println("cache")

    json_error := json.Unmarshal(cache, &r)
    check(json_error)

  } else  {
    println("no cache")
    token, err := ioutil.ReadFile(home + "/.gitlab-token")
    check(err)

    url := "https://gitlab.com/api/v3/projects/starred?per_page=100&private_token=" + strings.TrimSpace(string(token))

    get, err := http.Get(url)
    check(err)
    defer get.Body.Close()

    body, err := ioutil.ReadAll(get.Body)
    check(err)

    json_error := json.Unmarshal([]byte(body), &r)
    check(json_error)

    file_error := ioutil.WriteFile(cache_path, []byte(body), 0777)
    check(file_error)
  }

  for _, repo := range r {
    if strings.Contains(repo.Name, string(program_args)) {

      item := alfred.ResponseItem {
        Valid: true,
        UID: "",
        Title: repo.Name,
        Subtitle: repo.Description,
        Arg: repo.WebUrl,
      }
      resp.AddItem(&item)

    }
  }

  xml, err := resp.ToXML()
  check(err)

  fmt.Println(xml)
  os.Exit(0)

}
