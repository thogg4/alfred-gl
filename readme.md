# Alfred gl

### How to use
* create ~/.gitlab-token file that contains only the oauth token assigned to the account
* run install commands:
``` ruby
rake bundle:install
rake install
```
* open alfred and trigger with the `gl` keyword
* search for your starred repos

### Contributing
* fork
* add feature
* submit pull request
